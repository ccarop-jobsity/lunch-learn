
def hola_mundo():
    """Hola mundo."""
    my_string = "Hola mundo."
    print ("\n")
    print ("Ejemplo 1: " + my_string)


def hola_mundo_mejorado():
    """Ejemplo de hola mundo utilizando replace."""
    my_string = "Hola mundo mejorado."
    print ("\n")
    print("Ejemplo 2: " + my_string.replace(".", "")
                                   .replace(" mejorado", "!!!").lower())


def hola_mundo_separado():
    """Lists."""
    my_array = "Esto,es,un,string.".split(",")
    print ("\n")
    print("Ejemplo 3:" + str(my_array))
    print("Recorriendo ejemplo 3: ")

    my_string_1 = ""
    for element in my_array:
        my_string_1 += element + " "
    my_string_1 = my_string_1.strip()

    # OR the 1 line way.:
    my_string_2 = " ".join(my_array)

    print (my_string_1)
    print (my_string_1 == my_string_2)

    # Is 1 better than 4.... Sometimes.

def mundo_mejorado():
    my_string = "hola mundo mejorado."
    print ("\n")
    print("Ejemplo 4:")
    print(my_string[5:])
    print(my_string[:10])

    #Lo quiero en Capitals.
    print(my_string[:1].upper() + my_string[1:])

    #Built in.
    print(my_string.capitalize())

    #Pueden ser negativos:
    print(my_string[:-5])
    print(my_string[-10:])
    print(my_string[:-20000])

def por_ultimo_pero_no_menos_importante_el_famoso_el_impelable():
    my_string = "hola mundo mejorado."
    print(len(my_string))


""" Build in string methods.
s.lower(), s.upper()
s.strip()
s.isalpha()/s.isdigit()/s.isspace()
s.startswith('other'), s.endswith('other')
s.find('other')
s.replace('old', 'new')
s.split('delim')
s.join(list) together using the string as the delimiter. e.g. ''ccc']) -> aaa
"""

if __name__ == '__main__':
    hola_mundo()
    #hola_mundo_mejorado()
    #hola_mundo_separado()
    #mundo_mejorado()
    #por_ultimo_pero_no_menos_importante_el_famoso_el_impelable()
