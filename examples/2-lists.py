# Los mutables siempre son apuntadores.
import copy

def hola_mundo():
    my_string = ["hola", "mundo"]
    my_string_copy_fail = my_string
    my_string[0] = "FAIL"
    print("Ejemplo 1: ")
    print(my_string)
    print(my_string_copy_fail)

def hola_mundo_suc():
    # Mostrar copy y deep copy.
    my_string = ["hola", "mundo"]
    my_string_copy_fail = my_string[:]
    my_string[0] = "FAIL"
    print("\n Ejemplo 2: ")
    print(my_string)
    print(my_string_copy_fail)

def hola_mundo_separado():
    my_string = ["hola", "mundo"]
    final_word = ""
    print("Ejemplo 3: ")
    for word in my_string:
        final_word += word + " "
    print(final_word)

    # Recordemos que si son strings!
    # my_string_2 = " ".join(my_array)

    # Otros lenguajes antes de que se copiaran de python :) :
    # final_word = ""
    # for i in range(0, len(my_string), 1):
    #     final_word += my_string[i] + " "
    # print(final_word)




"""Build in lists methods.
list.append(elem)
list.insert(index, elem)
list.extend(list2) adds the elements in list2 to the end of the list. Using + or += on a list is similar to using extend().
list.index(elem)
list.remove(elem)
list.sort()
list.reverse()
list.pop(index)
"""

if __name__ == '__main__':
    hola_mundo()
    hola_mundo_suc()
    hola_mundo_separado()
    #mundo_mejorado()
    #por_ultimo_pero_no_menos_importante_el_famoso_el_impelable()
